var operandoa;
var operandob;
var operacion;

function init(){
    //variables
     var resultado = document.getElementById("resultado");
     var reset = document.getElementById("reset");
     var suma = document.getElementById("suma");
     var resta = document.getElementById("resta");
     var multiplicacion = document.getElementById("multiplicacion");
     var division = document.getElementById("division");
     var igual = document.getElementById("igual");
     var uno = document.getElementById("uno");
     var dos = document.getElementById("dos");
     var tres = document.getElementById("tres");
     var cuatro = document.getElementById("cuatro");
     var cinco = document.getElementById("cinco");
     var seis = document.getElementById("seis");
     var siete = document.getElementById("siete");
     var ocho = document.getElementById("ocho");
     var nueve = document.getElementById("nueve");
     var cero = document.getElementById("cero");
    var sqrt = document.getElementById("sqrt");
    var mod = document.getElementById("mod");
    var inver = document.getElementById("unoentre");
    var punto= document.getElementById("punto");
    var memoria;
    var mc=document.getElementById("mc");
    var mr=document.getElementById("mr");
    var ms=document.getElementById("ms");
    var mmas=document.getElementById("mmas");
    var ce=document.getElementById("ce");
    var backs=document.getElementById("backspace");
    var  cajita=document.getElementById("memoria")
    
    
    
    //eventos
    
    uno.onclick=function(e){
        resultado.textContent=resultado.textContent+"1";
    }
    dos.onclick=function(e){
        resultado.textContent=resultado.textContent+"2";
    }
    
    tres.onclick=function(e){
        resultado.textContent=resultado.textContent+"3";
    }
    
    cuatro.onclick=function(e){
        resultado.textContent=resultado.textContent+"4";
    }
    
    cinco.onclick=function(e){
        resultado.textContent=resultado.textContent+"5";
    }
    
    seis.onclick=function(e){
        resultado.textContent=resultado.textContent+"6";
    }
    
    siete.onclick=function(e){
        resultado.textContent=resultado.textContent+"7";
    }
    
    ocho.onclick=function(e){
        resultado.textContent=resultado.textContent+"8";
    }
    
    nueve.onclick=function(e){
        resultado.textContent=resultado.textContent+"9";
        
    }
    
    cero.onclick=function(e){
        resultado.textContent=resultado.textContent+"0";
    }
    
    reset.onclick = function(e){
      resetear();
    }
    
    suma.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "+";
      limpiar();
    }
   
    resta.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "-";
      limpiar();
    }
   
    multiplicacion.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "*";
      limpiar();
    }
   
    division.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "/";
      limpiar();
    }
    
    mod.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "%";
      limpiar();
    }
    
   sqrt.onclick = function(e){
      operandoa=resultado.textContent;
      res = Math.sqrt(operandoa);
        resetear();
  resultado.textContent = res;

    }
   inver.onclick = function(e){
       operandoa=resultado.textContent;
      res = 1/operandoa;
        resetear();
  resultado.textContent = res;
   }
    
  
    igual.onclick = function(e){
      operandob = resultado.textContent;
      resolver();
    }
    
    punto.onclick = function(e){
        cadena = resultado.textContent;
        
        if(cadena.indexOf('.') != -1){
alert("No meta mas puntos, no se puede");
}else{
    resultado.textContent = resultado.textContent + "." ;
}
    }
    
    mc.onclick = function(e){
        
        memoria = 0;
        cajita.textContent=memoria;

    }
    
        mr.onclick = function(e){
        
                resetear1();
        resultado.textContent=memoria;
            
        
    }
    
    ms.onclick = function(e){
        
                resetear1();
        memoria=parseFloat(resultado.textContent);
        cajita.textContent=memoria;
       
     
    }
    
    mmas.onclick = function(e){
            resetear1();
        memoria = memoria + parseFloat(resultado.textContent);
        cajita.textContent=memoria;
    }
    
    ce.onclick = function(e){
            cadena2=resultado.textContent;
            resultado.textContent=cadena2.substring(0,cadena2.length-1);
    }
    backs.onclick = function(e){
         cadena2=resultado.textContent;
            resultado.textContent=cadena2.substring(0,cadena2.length-1);
    }
    

}

function resolver(){
        var res = 0;
            switch(operacion){
                case "+":
                    res = parseFloat(operandoa) + parseFloat(operandob);
                 break;
                case "-":
                    res = parseFloat(operandoa) - parseFloat(operandob);
                 break;
                case "*":
                    res = parseFloat(operandoa) * parseFloat(operandob);
                 break;
                case "/":
                    res = parseFloat(operandoa) / parseFloat(operandob);
                 break;
                    
                case "%":
                    res = parseFloat(operandoa) % parseFloat(operandob);
                 break;
                    
                      
                    
  }
  resetear();
  resultado.textContent = res;
}

function limpiar(){
        resultado.textContent = "";
}

function resetear(){
  resultado.textContent = "";
  operandoa = 0;
  operandob = 0;
  operacion = "";
}
function resetear1(){

  operandoa = 0;
  operandob = 0;
  operacion = "";
}